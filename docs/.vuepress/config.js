const navConf = require("./config/nav");
const sidebarConf = require("./config/sidebar");
const pluginsConf = require("./config/plugins/index");
const dayjs = require("dayjs");
module.exports = {
    //注意，此处需要填写你部署在nginx下的文件夹名称，如果是根目录，那么可以注释掉此行，注释掉后本地打开index.html无法访问
    // base: "/dist/",
    title: "Fade away",
    description: "花有重开日，人无在少年",
    dest: "./dist",
    port: "6210",
    head: [
        [
            "script",
            {
                type: "text/javascript",
                src: "https://cdn.jsdelivr.net/npm/animejs@3.0.1/lib/anime.min.js",
            },
        ],
        ["script", { type: "text/javascript", src: "/js/mouseClick.js" }],
        ["link", { rel: "icon", href: "/img/favicon.ico" }],
        [
            "meta",
            {
                name: "keywords",
                content:
                    "前端笔记，vue,react,webpack,canvas,webgl,前端三剑客,Fade away,花有重开日,人无再少年",
            },
        ],
        [
            "meta",
            {
                name: "description",
                content: "前端笔记，vue,react,webpack,canvas,webgl",
            },
        ],
        [
            "meta",
            {
                name: "viewport",
                content: "width=device-width,initial-scale=1,user-scalable=no",
            },
        ],
        ["meta", { name: "robots", content: "all" }],
        ["meta", { name: "author", content: "xiazhi" }],
        ["link", { rel: "stylesheet", href: "/css/style.css" }],
    ],
    theme: "reco",
    themeConfig: {
        mode: "light",
        modePicker: false,
        smoothScroll: true,
        lastUpdated: dayjs().format("YYYY-MM-DD"), // string | boolean
        author: "夏至",
        // 项目开始时间
        startYear: "2022",
        nav: navConf,
        sidebar: sidebarConf,
        sidebarDepth: 2,
        // 自动形成侧边导航
        sidebar: "auto",
        // logo: '/head.png',
        // 搜索设置
        search: true,
        searchMaxSuggestions: 10,
        noFoundPageByTencent: false,
        codeTheme: "tomorrow",
    },
    markdown: {},
    plugins: pluginsConf,
    // plugins: [
    //     (pluginOptions, context) => {
    //         console.log(context)
    //         return {
    //             name: "my-xxx-plugin",
    //             // ... the rest of options
    //         };
    //     },
    // ],
};

