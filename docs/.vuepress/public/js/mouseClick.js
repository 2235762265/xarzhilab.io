window.onload = function () {
    // 导航栏logo动画
    const siteName = document.querySelector(".site-name");
    const keyframs = new KeyframeEffect(
        siteName,
        [
            { transform: "rotate(0)" },
            { transform: "rotate(30deg)" },
            { transform: "rotate(-30deg)" },
            { transform: "rotate(15deg)" },
            { transform: "rotate(-15deg)" },
            { transform: "rotate(0)" },
        ],
        {
            delay: 0,
            duration: 1000,
        }
    );
    const ani = new Animation(keyframs);
    siteName.addEventListener("mouseover", () => {
        ani.play();
    });




    
};
