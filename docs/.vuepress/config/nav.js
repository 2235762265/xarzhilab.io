module.exports = [
    {
        text: "前端三剑客",
        link: "/前端三剑客",
        icon: "reco-document",
        items: [
            {
                text: "Html",
                items: [{ text: "Html", link: "/前端三剑客/Html/01.Html初识" }],
            },
            {
                text: "CSS",
                items: [
                    { text: "CSS", link: "/前端三剑客/CSS/CSS/01.引入css样式表" },
                    { text: "CSS3", link: "/前端三剑客/CSS/CSS3/01.新增选择器" },
                ],
            },
            {
                text: "JavaScript",
                items: [
                    {
                        text: "Js基础",
                        link: "/前端三剑客/javascript/js基础/01.js基础语法",
                    },
                    {
                        text: "Js高级",
                        link: "/前端三剑客/javascript/js高级/01.this关键字",
                    },
                ],
            },
        ],
    },
    {
        text: "框架",
        link: "/框架",
        items: [
            {
                text: "Vue",
                items: [
                    { text: "Vue2", link: "/框架/Vue/Vue2/01.Vue基础" },
                    { text: "Vue3", link: "/框架/Vue/Vue3/1.创建Vue3项目" },
                ],
            },
            {
                text: "React",
                items: [{ text: "React", link: "/框架/React/01.React入门" }],
            },
            {
                text: "小程序",
                items: [
                    { text: "mina", link: "/框架/小程序/01.mina" },
                    { text: "uni-app", link: "/框架/小程序/02.uni-app" },
                ],
            },
        ],
    },
    {
        text: "Node.js",
        link: "/Node.js/01.初识Node.js",
    },
    {
        text: "Webpack",
        link: "/Webpack/01.Webpack基础.md",
    },
    {
        text: "学习git",
        link: "/学习git/git学习",
    },
    {
        text: "可视化",
        link: "/可视化/Canvas/01.认识Canvas",
        items: [
            { text: "Canvas", link: "/可视化/Canvas/01.认识Canvas" },
            { text: "WebGl", link: "/可视化/WebGl/01.WebGL基础" },
            { text: "Three.js", link: "/可视化/Three.js/01.基础用法" },
        ],
    },
    // {
    //   text: "面试题",
    //   link: "/面试题/01.Html+Css面试题",
    // },
    {
        text: "C++",
        link: "/C++/01.环境配置",
    },
    {
        text: "收藏网址",
        link: "/收藏的网站/收藏的网站",
    },
];
