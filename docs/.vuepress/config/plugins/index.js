const path = require("path");
module.exports = [
    [
        "@vuepress-reco/vuepress-plugin-kan-ban-niang",
        {
            theme: ["wanko", "miku", "z16"],
            clean: true,
            modelStyle: { left: "250px", bottom: "0px", opacity: "0.9" },
            btnStyle: { left: "380px", bottom: "20px" },
        },
    ],
    [
        "@vuepress/pwa",
        {
            serviceWorker: true,
            updatePopup: {
                message: "有新的内容更新",
                buttonText: "刷新",
            },
        },
    ],
    [
        "vuepress-plugin-auto-sidebar",
        {
            // collapsable: true,
            // titleMode: "titlecase",
            sidebarDepth: 3,
        },
    ],
    [
        "@vuepress/medium-zoom",
        {
            selector: ":not(a) > img",
            options: {
                margin: 16,
            },
        },
    ],
    [
        "dynamic-title",
        {
            showIcon: "/img/favicon.ico",
            showText: "(/≧▽≦/)欢迎回来！",
            hideIcon: "/img/favicon.ico",
            hideText: "(●—●)哦吼,不要走,给个收藏吧！",
            recoverTime: 2000,
        },
    ],
    [
        "@vuepress-reco/vuepress-plugin-rss",
        {
            site_url: "https://xarzhi.gitee.io", //网站地址
            copyright: "xarzhi", //版权署名
        },
    ],
    [
        "sakura",
        {
            //樱花插件
            num: 20, // 默认数量
            show: true, //  是否显示
            zIndex: -1, // 层级
            img: {
                replace: false, // false 默认图 true 换图 需要填写httpUrl地址
                httpUrl: "...", // 绝对路径
            },
        },
    ],
    ["@vuepress-reco/back-to-top", false],
    "@vuepress/nprogress",
    "vuepress-plugin-smooth-scroll",
    "reading-progress",
    "go-top",
    "vuepress-plugin-code-copy",

    () => {
        return {
            name: "background-change",
            clientRootMixin: path.resolve(__dirname, "./background-change/clientRootMixin.js"),
        };
    },
    () => {
        return {
            name: "mouse-click-particles",
            clientRootMixin: path.resolve(__dirname, "./mouse-click-particles/clientRootMixin.js"),
        };
    },
];
