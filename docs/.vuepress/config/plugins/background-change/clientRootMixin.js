export default {
    mounted() {
        const imgadd = [
            "/img/bg/bg.png",
            "/img/bg/bg1.jpeg",
            "/img/bg/bg2.jpeg",
            "/img/bg/bg3.jpeg",
            "/img/bg/bg4.jpeg",
            "/img/bg/bg5.jpeg",
            "/img/bg/bg6.jpeg",
            "/img/bg/bg7.jpeg",
            "/img/bg/bg8.jpeg",
            "/img/bg/bg9.jpeg",
            "/img/bg/bg10.jpeg",
            "/img/bg/bg12.jpeg",
            "/img/bg/bg13.jpg",
        ];
        const getRandom = (min, max) => {
            return Math.floor(Math.random() * (max - min + 1) + min);
        };
        document.body.style.background = `url(${imgadd[getRandom(0, imgadd.length - 1)]}) no-repeat`;
        document.body.style.backgroundSize = "cover";
        document.body.style.backgroundAttachment = "fixed";
    },
};
